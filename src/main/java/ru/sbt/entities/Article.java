package ru.sbt.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;

@Entity
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    @Column
    private String title;
    @Column
    private Timestamp publicationDate;
    //    @Column(nullable = false)
    @Column
    private String authorsId;
    @Column
    private String text;


    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public Article(
            String authorsId,
            String title,
            Timestamp publicationDate,
            String text) {

        this.title = title;
        this.publicationDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
        this.authorsId = authorsId;
        this.text = text;
    }

    public Article() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Timestamp publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getAuthorsId() {
        return authorsId;
    }

    public void setAuthorsId(String authorsId) {
        this.authorsId = authorsId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
